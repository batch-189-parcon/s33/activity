// alert('waddup')
//https://jsonplaceholder.typicode.com/todos

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});


/*
MAP METHOD
*/


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
})
.then((response) => response.json())
.then((data) => console.log(data))


/*
	STEP 6

*/


fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title:'Created to do List Item',
		userId: 1,
		completed: false
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		status: 'Pending',
		userId: 1,
		description: "To update the my to do list with different structure",
		dateCompleted: 'Pending'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		status: "Complete",
		userId: 1,
		description: "To update the my to do list with different structure",
		dateCompleted: "06/29/21"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// .then((data) => {var d1 = new Date()
// 	console.log(d1)})



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))